<div class="row empresaPage">
	<div class="row destaque">
		<div class="linha"></div>
		<div class="container">
			<div class="col-lg-3 col-md-3">
				<h1><?php the_title();?></h1>
			</div>
		</div>
	</div>
	<div class="row adv">
		<div class="container">
			<span class="shadow"></span>
			<div class="banner">
				<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'banner-empresa', 'order' => 'ASC' );
						$myposts = get_posts($args);
						$i = 0;
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
				?>
				<img src="<?=$destaque;?>" class="img-responsive"/>
				<?php the_content();?>
				<?php endforeach; 
				wp_reset_postdata();?>
			</div>
		</div>
	</div>
	<div class="row empresa">
		<div class="container">
			<div class="col-lg-12 col-md-12">
				<?php the_content();?>
			</div>
			<div class="img-empresa col-lg-12 col-md-12">
				<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'imagem-empresa', 'order' => 'ASC' );
						$myposts = get_posts($args);
						$i = 0;
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
				?>
				<div class="img col-lg-4 col-md-4 text-center">
					<img src="<?=$destaque;?>"/>
					<p><?php the_title();?></p>
				</div>
				<?php endforeach; 
				wp_reset_postdata();?>
			</div>
		</div>
	</div>
</div>