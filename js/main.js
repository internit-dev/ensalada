/**
 * Main Javascript.
 * This file is for who want to make this theme as a new parent theme and you are ready to code your js here.
 */
 jQuery(document).ready(function() {
	 
	 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		    m=s.getElementsByTagName(o)
		    [0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		    ga('create', 'UA-28302586-1', 'auto') ; 

		    ga('send', 'pageview');
	 
	 jQuery("#banner-index").owlCarousel({
	      navigation : true,
	      slideSpeed : 300,
	      paginationSpeed : 400,
	      singleItem:true,
	      navigation: false,
	      items:1,
	      nav: true
	  });
	 
	 jQuery("#owl-clientes").owlCarousel({
	      autoPlay: 3000, 
	      items : 4,
	      itemsDesktop : [1199,4],
	      itemsDesktopSmall : [979,4]
	  });

	 jQuery("#owl-cases").owlCarousel({
	      autoPlay: 5000, 
	      items : 5,
	      itemsDesktop : [1199,4],
	      itemsDesktopSmall : [979,4],
	      navigation: true
	  });
	 
	jQuery("#tabelaPat").find("table").addClass("table");
	
	var ativo = jQuery('#owl-cases .item.active');
	jQuery('#owl-cases .item').on("click", function(){
		itemClicado = jQuery(this);
		itemClicado.addClass("active");
		ativo.removeClass("active");
		ativo = itemClicado;
	});
	
	jQuery(".casesClientes .owl-prev").html("<span class='setinhaEsquerda' style='display:inline-block'></span>");
	jQuery(".casesClientes .owl-next").html("<span class='setinhaDireita' style='display:inline-block'></span>");
	
	jQuery(".topo .tel").attr("onClick", "ga('send', 'event', 'telefone', 'click', 'telefone')");
	jQuery(".rodape .contato button.contato").attr("onClick", "ga('send', 'event', 'telefone', 'click', 'telefone')");
	
	jQuery(".topo .navbar-default .navbar-nav>li>a").each(function(){
		if(jQuery(this).text() == "Peça seu orçamento"){
			jQuery(this).attr("onClick", "ga('send', 'event', 'orcamento', 'click', 'orcamento')");
		}else if(jQuery(this).text() == "Contato"){
			jQuery(this).attr("onClick", "ga('send', 'event', 'orcamento', 'click', 'contato')");
		}
	});
	
	jQuery(document).scroll(function(){
	    if(jQuery(this).scrollTop() > 50)
	    {   
	    	jQuery('.topo').addClass("ativo");
	        //.css({"background-image":"url('"+root+"img/textura.png')"});
	      
	    } else {
	    	jQuery('.topo').removeClass("ativo");
	    	//css({"background-color":"#fff"});
	    	
	    }
	});
	
	jQuery( '#h-tel' ).click(function() {
		 fbq('track', 'clique_telefone', {
		   content_name: 'Ensalada visitou Telefone',
		   value: 0.50,
		   currency: 'USD'
		  });
		});

	jQuery( '#f-mapa' ).click(function() {
		 fbq('track', 'clique_mapa', {
		   content_name: 'Ensalada visitou Mapa',
		   value: 0.50,
		   currency: 'USD'
		  });
		});

	jQuery( '#f-tel' ).click(function() {
		 fbq('track', 'clique_telefone', {
		   content_name: 'Ensalada visitou Telefone',
		   value: 0.50,
		   currency: 'USD'
		  });
		});

	jQuery( '#enviar-contato' ).click(function() {
		 fbq('track', 'enviar_formulario_contato', {
		   content_name: 'Ensalada enviou formulario de contato',
		   value: 0.50,
		   currency: 'USD'
		  });
		});

	jQuery( '#ligamos-header' ).click(function() {
		 fbq('track', 'clique_ligamos_header', {
		   content_name: 'Ensalada visitou Ligamos para você',
		   value: 0.50,
		   currency: 'USD'
		  });
		});

});
