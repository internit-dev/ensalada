<?php
/**
 * Template Name: Eventos
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();

?> 
			<?php 
			while (have_posts()) {
				the_post();
		
				get_template_part('content', 'eventos');
		
			} //endwhile;
			?>

<?php get_footer(); ?> 