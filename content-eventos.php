<div class="row eventosPage">
	<div class="row destaque">
		<div class="linha"></div>
		<div class="container">
			<div class="col-lg-12 fundoPersonalizado">
				<h1>Serviços</h1>
				<h2><?php the_title()?></h2>
				<span></span>
			</div>
		</div>
	</div>
	<div class="row banner">
		<div class="container">
			<div class="col-lg-12 noRightPadding">
				<?php $destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));?>
				<img src="<?=$destaque;?>"  class="img-responsive"/>
			</div>
		</div>
	</div>
	<div class="row more">
		<div class="container">
			<div class="col-lg-11 col-md-11">
				<?php the_content();?>
			</div>
		</div>
	</div>
</div>