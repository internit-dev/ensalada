<div class="row banner">
				<div id="banner-index" class="owl-carousel owl-theme">
					<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'banner-home', 'order' => 'ASC' );
						$myposts = get_posts($args);
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
						 ?>
						 	<div class="item"><img src="<?=$destaque;?>" alt=""></div>
						<?php endforeach; 
						wp_reset_postdata();?>
				</div>
			</div>
			<div class="row conteudoIndex">
				<div class="container">
					<h1>Nossos Serviços</h1>
				</div>
			</div>
			<div class="row servicosIndex">
				<div class="container">
					<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'servicos', 'order' => 'ASC' );
						$myposts = get_posts($args);
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
							
						 ?>
						<div class="col-lg-3 col-md-5 itemServico">
							<span class="icone"></span>
							<h1 class="text-uppercase"><?php the_title();?></h1>
							<a href="<?= bloginfo('url')."/".get_the_content(); ?>"><span class="glyphicon glyphicon-plus"></span></a>
						</div>
						<?php endforeach; 
						wp_reset_postdata();?>
				</div>
			</div>