<div class="row cases">
	<div class="row destaque">
		<div class="linha"></div>
		<div class="container">
			<div class="col-lg-3 col-md-3">
				<h1><?php the_title();?></h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="container">
			<div class="col-lg-12 casesClientes">
				<ul class="nav nav-tabs">
					<div id="owl-cases">
					<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'cases-itens', 'order' => 'ASC' );
						$myposts = get_posts($args);
						$i = 0;
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
							$classe = "";
					?>
					
					<?php if($i == 0){$classe="active";}?>
						<div class="item <?=$classe?>" data-toggle="tab" href="#menu<?php echo $i;?>"><li class="<?=$classe?>"><a data-toggle="tab" href="#menu<?php echo $i;?>"><img src="<?=$destaque;?>"></a></li></div>
					<?php $i++;?>
					<?php endforeach; 
					wp_reset_postdata();?>
					</div>
				</ul>
				<div class="tab-content">
					<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'cases-itens', 'order' => 'ASC' );
						$myposts = get_posts($args);
						$i = 0;
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
					?>
						<div id="menu<?php echo $i?>" class="tab-pane fade in <?php if($i === 0){ echo "active";}?>">
							<div class="col-lg-12">
								<h1><?php the_title();?></h1>
							    <hr>
							    <p><?php the_content();?></p>
							</div>
						</div>
					<?php $i++;?>
					<?php endforeach; 
					wp_reset_postdata();?>
				</div>
			</div>
		</div>
	</div>
</div>