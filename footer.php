﻿<?php
/**
 * The theme footer
 * 
 * @package bootstrap-basic
 */
?>
		<div class="row rodape">
				<div class="container">
					<div class="col-lg-4 col-xs-12 col-md-3 col-sm-3 compartilhe">
						<!-- <span class="simbolo"></span> -->
						<p class="text-uppercase">Redes Sociais:</p>
						<div class="row">
							<div class="col-lg-2 col-md-12 col-lg-offset-1 col-sm-12 col-xs-12 col-xs-offset-0 redesSociais">
								<!-- <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
									<a href="https://www.facebook.com/ensalada.rc/" class="addthis_button_facebook at300b"><span class="facebook"></span></a>
								</div> -->
								<!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
									<a href="#" class="addthis_button_twitter at300b"><span class="twitter"></span></a>
								</div>-->
								<div class="col-lg-6 col-md-4 col-sm-4 col-xs-3">
									<a href="https://www.facebook.com/ensalada.rc/" target="_blank"><span class="facebook"></span></a>
								</div>
								<div class="col-lg-6 col-md-4 col-sm-4 col-xs-3">
									<a href="https://www.linkedin.com/company-beta/11104880/" target="_blank"><span class="linkedin"></span></a>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-lg-offset-1 col-sm-12 col-xs-12 col-xs-offset-0 fb">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fensalada.rc%2F&width=450&layout=
							standard&action=like&size=small&show_faces=true&share=true&height=80&appId" 
							width="100%" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
						<!--<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fensalada.rc%2F&width=450&layout=
						standard&action=like&size=small&show_faces=true&share=true&height=80&appId" 
						width="100%" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>-->
					</div>
					<div class="col-lg-4 col-xs-12 col-md-4 col-sm-4 localizacao">
						<span></span>
						<p class="text-uppercase">Onde Estamos</p>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 no-padding-mobile">
									<div class="col-lg-8 col-md-8 col-sm-12 no-padding-mobile">
										<p>Rua Zuleika Brazil Silva, nº 07 - Fonseca - Niterói, Rj</p>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12">
									<button class="btn mapa" id="f-mapa" data-toggle="modal" data-target="#mapa">Ver o Mapa</button>
								</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-xs-12 col-md-5 col-sm-4 contato">
						<span></span>
						<p class="text-uppercase">Entre em contato</p>
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-12 col-lg-offset-1">
								<button class="btn form-control contato" id="f-tel" data-toggle="modal" data-target="#telefonesContato">Clique para ver nossos telefones</button>
							</div>
						</div>
						<img src="<?php bloginfo('template_url'); ?>/img/assinatura_internit.png"/>
					</div>
				</div>
			</div>
		</div>
		<!--wordpress footer-->
		<?php wp_footer(); ?> 
		<!-- Modal -->
		<div id="telefonesContato" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h3 class="modal-title text-center"><strong>Telefones de Contato</strong></h3>
		      </div>
		      <div class="modal-body">
		        <ul>
		        	<li>(21) 2625-8801 / (21) 2625-8901 / (21) 2625-8721</li>
		        </ul>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
		      </div>
		    </div>
		  </div>
		</div>
		
		<!-- Modal -->
		<div id="mapa" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h3 class="modal-title text-center"><strong>Onde Estamos</strong></h3>
		      </div>
		      <div class="modal-body">
		        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.9149297634485!2d-43.098414084448564!3d-22.879600742654937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99847f6e8a2289%3A0xba91b1765549d8fc!2sEnsalada+-+Refei%C3%A7%C3%B5es+coletivas+e+quentinhas+em+Niter%C3%B3i+e+RJ.!5e0!3m2!1spt-BR!2sbr!4v1464369937076" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
		      </div>
		    </div>
		  </div>
		</div>

		<!-- Modal Ligamos para você-->
		<div id="ligamos_para_voce" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h3 class="modal-title text-center"><strong>Ligamos para você!</strong></h3>
		      </div>
		      <div class="modal-body">
		      	<div class="col-lg-12">
		        	<?php echo do_shortcode('[contact-form-7 id="1165" title="Ligamos para você"]');?>
		        </div>
		      </div>
		      <div class="modal-footer">
		       
		      </div>
		    </div>
		  </div>
		</div>
		<script src="<?php bloginfo('template_url'); ?>/js/vendor/addthis.js"></script>
	</body>
</html>