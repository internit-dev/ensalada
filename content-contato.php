<div class="row contatoPage">
	<div class="row destaque">
		<div class="linha"></div>
		<div class="container">
			<div class="col-lg-3 col-md-3">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
	<div class="row adv">
		<div class="container">
			<span class="shadow"></span>
			<h1 class="text-center"><?php the_content(); ?></h1>
		</div>
	</div>
	<div class="row formulario">
		<div class="container">
			<div class="col-lg-10 col-lg-offset-1">
				<?php echo do_shortcode('[contact-form-7 id="24" title="Contato"]');?>
			</div>
		</div>
	</div>
</div>