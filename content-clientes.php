<div class="row clientesPage">
	<div class="row destaque">
		<div class="linha"></div>
		<div class="container">
			<div class="col-lg-3 col-md-3">
				<h1><?php the_title();?></h1>
			</div>
		</div>
	</div>
	<div class="row clientesItens">
		<div class="container">
			<?php
				$args = array( 'posts_per_page' => -1, 'category_name' => 'clientes', 'order' => 'ASC' );
				$myposts = get_posts($args);
				$i = 0;
				foreach ($myposts as $post) : setup_postdata($post);
					$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
			?>
			<a href="<?= get_the_content();?>" target="_blank">
				<div class="col-lg-4 col-md-4">
					<div class="item">
						<img src="<?= $destaque;?>">
					</div>
					<p><?php the_title();?></p>
				</div>
			</a>
			<?php endforeach; 
			wp_reset_postdata();?>
		</div>
	</div>
</div>