<?php
/**
 * The theme header
 * 
 * @package bootstrap-basic
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title('|', true, 'right'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<link rel="icon" href="<?php bloginfo('template_url'); ?>/img/favicon.png" />
		<!--wordpress head-->
		<?php wp_head(); ?>
		<!-- Facebook Pixel Code -->
			<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '314602835608863'); // Insert your pixel ID here.
			fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none" 
			src="https://www.facebook.com/tr?id=314602835608863&ev=PageView&noscript=1" 
			/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->
	</head>
	<body <?php body_class(); ?>>
	<div class="container-fluid">
		<!--[if lt IE 8]>
			<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
		<![endif]-->
			<div class="row topo">
					<hr class="destacada">
					<div class="container anteMenu col-sm-12 col-xs-12 info">
						<div class="logo"><a href="<?php echo get_site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" class="img-responsive"></a></div>
						<div class="col-lg-5 col-lg-offset-7 col-xs-12 col-xs-offset-0 col-md-7 col-md-offset-5 segura">

							<a class="facebook" href="https://www.facebook.com/ensalada.rc/" target="_blank" align="center"><span></span></a>
							<a class="linkedin" href="https://www.linkedin.com/company-beta/11104880/" target="_blank" align="center"><span></span></a>
							<button class="btn text-uppercase tel" id="ligamos-header" data-toggle="modal" data-target="#ligamos_para_voce"><span class="glyphicon glyphicon-earphone"></span> Ligamos para você</button>
							<button class="btn text-uppercase tel" id="h-tel"  data-toggle="modal" data-target="#telefonesContato"><span class="glyphicon glyphicon-earphone"></span> Clique para ver nossos telefones</button>
						</div>
					</div>
					<hr class="simples">
					<div class="col-lg-7 col-lg-offset-4 col-sm-12 col-xs-12 col-xs-offset-0 col-md-9 col-md-offset-3">
						<nav class="navbar navbar-default">
						  <div class="container-fluid">
						    <div class="navbar-header">
						      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span> 
						      </button>
						    </div>
						    <div class="collapse navbar-collapse" id="myNavbar">
						      <ul class="nav navbar-nav">
						        <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav', 'walker' => new BootstrapBasicMyWalkerNavMenu())); ?> 
						      </ul>
						    </div>
						  </div>
						</nav>
					</div>
				</div>