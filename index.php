<?php get_header();?>
<div class="row banner">
				<div id="banner-index" class="owl-carousel owl-theme">
					<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'banner-home', 'order' => 'ASC' );
						$myposts = get_posts($args);
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
						 ?>
						 	<div class="item"><img src="<?=$destaque;?>" alt=""></div>
						<?php endforeach; 
						wp_reset_postdata();?>
				</div>
			</div>
			<div class="row conteudoIndex">
				<div class="container">
					<h1>Nossos Serviços</h1>
				</div>
			</div>
			<div class="row servicosIndex">
				<div class="container">
					<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'servicos', 'order' => 'ASC' );
						$myposts = get_posts($args);
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
							
						 ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 itemServico">
							<span class="icone"></span>
							<h1 class="text-uppercase"><?php the_title();?></h1>
							<a href="<?= bloginfo('url')."/".get_the_content(); ?>"><span class="glyphicon glyphicon-plus"></span></a>
						</div>
						<?php endforeach; 
						wp_reset_postdata();?>
				</div>
			</div>
			<div class="row soliciteOrcamento">
				<div class="container">
					<a href="orcamento">
						<div class="col-lg-11 col-md-11 col-xs-12 box">
							<h1 class="text-uppercase">Solicite um <strong>orçamento</strong> agora</h1>
							<h2>Clique<br>Aqui</h2>
							<span class="setinha"></span>
						</div>
					</a>
				</div>
			</div>
			<div class="row ">
				<div class="container clientes">
					<div class="row">
						<a href="clientes" class="semFormat col-lg-2 col-md-2">
							<div class="destaque">
								<h1>Principais Clientes</h1>
							</div>
						</a>
						<div class="col-lg-8 col-md-8">
							<div id="owl-clientes">
								<?php
									$args = array( 'posts_per_page' => -1, 'category_name' => 'clientes-home', 'order' => 'ASC' );
									$myposts = get_posts($args);
									foreach ($myposts as $post) : setup_postdata($post);
										$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
								?>
						 		<div class="item cliente"><img src="<?=$destaque;?>" alt=""></div>
								<?php endforeach; 
								wp_reset_postdata();?>
							</div>
						</div>
						<a href="clientes" class="semFormat mais-clientes col-lg-2 col-md-2">
							<div class="destaque">
								<h1>Mais Clientes</h1>
							</div>
						</a>
					</div>
				</div>
			</div>
<?php get_footer(); ?> 