<div class="row patPage">
	<div class="row destaque">
		<div class="linha"></div>
		<div class="container">
			<div class="col-lg-12 fundoPersonalizado">
				<h1>Serviços</h1>
				<h2><?php the_title();?></h2>
				<span></span>
			</div>
		</div>
	</div>
	<div class="row banner">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php $destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
					<img src="<?=$destaque;?>" class="img-responsive"/>
				</div>
			</div>
		</div>
	</div>
	<div class="row more">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
					<?php the_content();?>
				</div>
				<div class="col-lg-5 col-md-5">
					<?php
							$args = array( 'posts_per_page' => -1, 'category_name' => 'divisao-pat', 'order' => 'ASC' );
							$myposts = get_posts($args);
							$i = 0;
							foreach ($myposts as $post) : setup_postdata($post);
						?>
						<?php the_content();?>
						<?php endforeach; 
						wp_reset_postdata();?>
					<div id="tabelaPat">
						<?php echo do_shortcode('[wptg_comparison_table id="1"]');?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>