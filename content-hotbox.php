<div class="row hotboxPage">
	<div class="row destaque">
		<div class="linha"></div>
		<div class="container">
			<div class="col-lg-12 fundoPersonalizado">
				<h1>Serviços</h1>
				<h2><?php the_title();?></h2>
				<span></span>
			</div>
		</div>
	</div>
	<div class="row banner">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php $destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
					<img src="<?=$destaque;?>" class="img-responsive"/>
					<p><?php echo get_post_meta($post->ID, 'descricao_img', true ); ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="row more">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<?php the_content();?>
				</div>
			</div>
			<div class="row imagens">
				<div class="container">
					<div class="col-lg-2 col-lg-offset-1 col-md-3">
						<h1>Mais Imagens</h1>
						<span></span>
					</div>
					<?php
						$args = array( 'posts_per_page' => -1, 'category_name' => 'hotbox', 'order' => 'ASC' );
						$myposts = get_posts($args);
						$i = 0;
						foreach ($myposts as $post) : setup_postdata($post);
							$destaque = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
					?>
					<div class="col-lg-3 col-md-3">
						<img src="<?=$destaque;?>"/>
						<p><?php the_title();?></p>
					</div>
					<?php endforeach; 
					wp_reset_postdata();?>
				</div>
			</div>
		</div>
	</div>
</div>