<?php
/**
 * Template for dispalying single post (read full post page).
 * 
 * @package bootstrap-basic
 */

get_header();

?> 
<div class="principal wow fadeInUp">
	<div class="container">
		<div class="row">
			<?php 
			while (have_posts()) {
				the_post();
		
				get_template_part('content', 'single');
		
			} //endwhile;
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?> 